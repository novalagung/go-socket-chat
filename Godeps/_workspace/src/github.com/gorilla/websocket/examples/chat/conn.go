// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// Modified by Noval Agung

package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"time"
)

const (
	// Time allowed to write a message to the peer.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the peer.
	pongWait = 60 * time.Second

	// Send pings to peer with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Maximum message size allowed from peer.
	maxMessageSize = 512
)

var (
	upgrader = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}

	// Connection counter
	connectionCounter int32 = 0
)

type ConnectionHeader struct {
	// random id generated when new connection is created
	Id string

	// alias of the connection, by default it'll be "Anonymous #"
	Alias string
}

// connection is an middleman between the websocket connection and the hub.
type connection struct {
	// Header of connection
	header ConnectionHeader

	// The websocket connection.
	ws *websocket.Conn

	// Buffered channel of outbound messages.
	send chan []byte
}

// readPump pumps messages from the websocket connection to the hub.
func (c *connection) readPump() {
	defer func() {
		h.unregister <- c
		c.ws.Close()
	}()
	c.ws.SetReadLimit(maxMessageSize)
	c.ws.SetReadDeadline(time.Now().Add(pongWait))
	c.ws.SetPongHandler(func(string) error { c.ws.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, message, err := c.ws.ReadMessage()
		if err != nil {
			if websocket.IsUnexpectedCloseError(err, websocket.CloseGoingAway) {
				log.Printf("error: %v", err)
			}
			break
		}

		h.broadcast <- message
	}
}

// write writes a message with the given message type and payload.
func (c *connection) write(mt int, payload []byte) error {
	c.ws.SetWriteDeadline(time.Now().Add(writeWait))
	return c.ws.WriteMessage(mt, payload)
}

// writePump pumps messages from the hub to the websocket connection.
func (c *connection) writePump() {
	// sent the connection id to client for the first time it's connected
	go func() {
		res := new(Response)
		res.Sender = c.header
		res.Type = MessageType_RegisterSubscriber
		bytes := ResponseToBytes(res)
		c.write(websocket.TextMessage, bytes)
	}()

	ticker := time.NewTicker(pingPeriod)
	defer func() {
		ticker.Stop()
		c.ws.Close()
	}()
	for {
		select {
		case message, ok := <-c.send:
			if !ok {
				c.write(websocket.CloseMessage, []byte{})
				return
			}
			if err := c.write(websocket.TextMessage, message); err != nil {
				return
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

// serveWs handles websocket requests from the peer.
func serveWs(w http.ResponseWriter, r *http.Request) {
	connectionCounter = connectionCounter + 1

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	c := new(connection)
	c.send = make(chan []byte, 256)
	c.ws = ws
	c.header = ConnectionHeader{
		Id: fmt.Sprintf("%d", time.Now().UnixNano()/int64(time.Millisecond)),
	}

	h.register <- c
	go c.writePump()
	c.readPump()
}

// get all connections
func getSubsribers(w http.ResponseWriter, r *http.Request) {
	headers := []ConnectionHeader{}
	for conn, _ := range h.connections {
		headers = append(headers, conn.header)
	}

	bytes, err := json.Marshal(headers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(bytes)
}

// if user wants to change the alias (the Id won't be changed, only the alias)
func changeAlias(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	id := r.FormValue("Id")
	newAlias := r.FormValue("Alias")

	w.Header().Set("Content-Type", "application/json")

	for conn, _ := range h.connections {
		if conn.header.Id == id {
			conn.header.Alias = newAlias
			w.Write([]byte(`true`))
			return
		}
	}

	w.Write([]byte(`false`))
}
