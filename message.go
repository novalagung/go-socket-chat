// Modified by Noval Agung

package main

import (
	"encoding/json"
)

type MessageType string

// type of the messages
const (
	MessageType_Broadcast          MessageType = "message"
	MessageType_Notify             MessageType = "notify"
	MessageType_RegisterSubscriber MessageType = "register subscriber"
	MessageType_GetAllSubscribers  MessageType = "get all subscriber"
)

// if you asking why the recivers is an array, even on the front end part its only single dropdown
// it's because this magic support multiple receivers!

// used to retreive message from the front end
type Request struct {
	Sender    string
	Receivers []string
	Type      MessageType
	Message   string
}

// used to send message to the front end
type Response struct {
	Sender   ConnectionHeader
	Receiver ConnectionHeader
	Type     MessageType
	Message  interface{}
}

// decode bytes to request (after receiving a message)
func BytesToRequest(bytes []byte) *Request {
	req := new(Request)
	if err := json.Unmarshal(bytes, req); err != nil {
		return nil
	}
	return req
}

// encode response to bytes (before sending it to the client)
func ResponseToBytes(res *Response) []byte {
	bytes, err := json.Marshal(*res)
	if err != nil {
		return nil
	}
	return bytes
}
