// Copyright 2013 The Gorilla WebSocket Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
// Modified by Noval Agung

package main

// hub maintains the set of active connections and broadcasts messages to the
// connections.
type hub struct {
	// Registered connections.
	connections map[*connection]bool

	// Inbound messages from the connections.
	broadcast chan []byte

	// Register requests from the connections.
	register chan *connection

	// Unregister requests from connections.
	unregister chan *connection
}

var needToNotifyTheSenderToo = true

var h = hub{
	broadcast:   make(chan []byte),
	register:    make(chan *connection),
	unregister:  make(chan *connection),
	connections: make(map[*connection]bool),
}

func (h *hub) getConnectionById(id string) *connection {
	for c := range h.connections {
		if c.header.Id == id {
			return c
		}
	}

	return nil
}

func (h *hub) run() {
	sent := func(c *connection, res *Response) {
		bytes := ResponseToBytes(res)
		select {
		case c.send <- bytes:
		default:
			close(c.send)
			delete(h.connections, c)
		}
	}
	for {
		select {
		case c := <-h.register:
			h.connections[c] = true
		case c := <-h.unregister:
			if _, ok := h.connections[c]; ok {
				delete(h.connections, c)
				close(c.send)
			}
		case m := <-h.broadcast:
			req := BytesToRequest(m)

			switch req.Type {
			// when the user load the page on the first time, he'll sent request using this type
			// with payload alias filled by user
			case MessageType_RegisterSubscriber:
				{
					for c := range h.connections {
						if c.header.Id == req.Sender {
							// set the alias of current connection id
							c.header.Alias = req.Message
						}
					}

					for c := range h.connections {
						// get all connections header except current
						headers := []ConnectionHeader{}
						for d := range h.connections {
							if c.header.Id != d.header.Id {
								headers = append(headers, d.header)
							}
						}

						// then distribute the headers to all connections
						// so they'll have capability to sent message to specific connection
						// (drop down will shown up on the front end)
						res := new(Response)
						res.Sender = ConnectionHeader{Id: req.Sender}
						res.Receiver = c.header
						res.Type = MessageType_GetAllSubscribers
						res.Message = headers

						sent(c, res)
					}
				}
			case MessageType_Broadcast:
				{
					connOfSender := h.getConnectionById(req.Sender)

					for c := range h.connections {
						// again, this sample support multiple receivers
						// (even not yet implemented on the UI part)
						// we have to loop the receivers
						for _, receiverId := range req.Receivers {
							connOfReceiver := h.getConnectionById(receiverId)

							// if user want to notify himself every time he sent a message
							if c.header.Id == connOfSender.header.Id && needToNotifyTheSenderToo {
								res := new(Response)
								res.Sender = connOfSender.header
								res.Receiver = connOfReceiver.header
								res.Type = MessageType_Notify
								res.Message = req.Message

								sent(c, res)
							}

							// sent the message to each receiver
							if c.header.Id == receiverId {
								res := new(Response)
								res.Sender = connOfSender.header
								res.Receiver = connOfReceiver.header
								res.Type = MessageType_Broadcast
								res.Message = req.Message

								sent(c, res)
							}
						}
					}
				}
			}
		}
	}
}
