# Chat

### How To Run

```go
go run *.go
```

### How To Deploy on Heroku

```go
git add .
git commit -a -m "ok"
godep save
go install
git push heroku master
```